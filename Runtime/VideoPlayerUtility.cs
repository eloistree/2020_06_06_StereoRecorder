﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;

public class VideoPlayerUtilityMono : MonoBehaviour
{
    public VideoPlayer m_videoPlayer;

    public void DownloadVideoFromUrlToDisk(string url, string absolutePath, DownloadVideoInfo info)
    {
        StartCoroutine(VideoPlayerUtility.Coroutine_DownloadVideoFromUrlToDisk(url, absolutePath, info));
    }
}

public class VideoPlayerUtility {
   
    public static long GetFrameByPourcent(VideoPlayer video,  float pourcent)
    {
        return (long)(pourcent * (float) video.frameCount);
    }

    internal static void SwitchPause(VideoPlayer video)
    {
        if (video.isPlaying) video.Pause();
        else video.Play();
    }
  

    public static float GetPourcentOfCurrentVideo(VideoPlayer video)
    {
        return video.frame / (float)video.frameCount;
    }

    public static  IEnumerator Coroutine_DownloadVideoFromUrlToDisk(string url,string absolutePath, DownloadVideoInfo downloadCallBack)
    {
        if(downloadCallBack==null)
            downloadCallBack = new DownloadVideoInfo();

        UnityWebRequest www = UnityWebRequest.Get(url);
        downloadCallBack.m_urlVideo = url;
        downloadCallBack.m_downloadPathGiven = absolutePath;

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            downloadCallBack.SetAsHasError();
        }
        else
        {

            File.WriteAllBytes(absolutePath, www.downloadHandler.data);
        }
        downloadCallBack.SetAsDownloaded();

        yield break;

    }
}
public class DownloadVideoInfo
{
    public bool m_hasError;
    public bool m_downloaded;
    public string m_urlVideo;
    public string m_downloadPathGiven;

    public void SetAsHasError()
    {
        m_hasError = true;
    }
    public void SetAsDownloaded() {
        m_downloaded = true;
    }
}