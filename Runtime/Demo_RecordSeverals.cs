﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Demo_RecordSeverals : MonoBehaviour
{
    public ScreenshotAroundObject [] m_recorders;
    public int indexFrame=0;
    public UnityEvent m_onfinished;
    public void StartRecording()
    {
        StartCoroutine(Coroutine_StartRecording());
    }
    public IEnumerator Coroutine_StartRecording()
    {
        for (int i = 0; i < m_recorders.Length; i++)
        {
            m_recorders[i].gameObject.SetActive(false);

        }
        for (int i = 0; i < m_recorders.Length; i++)
        {
            m_recorders[i].gameObject.SetActive(true);
            m_recorders[i].SetStartFrame(indexFrame);
            m_recorders[i].StayInPauseAfterRecord();
            yield return new WaitForEndOfFrame();

            m_recorders[i].TakeScreenshotsAround();
            while (!m_recorders[i].HasFinishRecording()) {
                yield return new WaitForEndOfFrame();
            }

            indexFrame += m_recorders[i].GetFramesCount();
            m_recorders[i].gameObject.SetActive(false);
        }
        m_onfinished.Invoke();
        yield break;
    }
}
