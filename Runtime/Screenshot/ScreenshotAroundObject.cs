﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScreenshotAroundObject : MonoBehaviour
{
    public ScreenshotManager m_screenManager;
    public StereoCameraTurnAroundManager m_turnAround;
    public float m_verticalAngle = 45;
    public int m_requestAngle = 720;
    public bool m_pauseGameWhileScreening = true;
    public bool m_exitPauseAfternSceeening = true;
    public string m_fileNameFormat = "Screenshot_";
    public int m_startIndex=0;
    public bool m_isRecording;


    public bool HasFinishRecording()
    {
        return !m_isRecording;
    }

    public void StayInPauseAfterRecord()
    {
        m_pauseGameWhileScreening = true;
        m_exitPauseAfternSceeening = false;
    }

    public ScreenshotStateEvent m_onChanged;

    public void SetStartFrame(int indexFrame)
    {
        m_startIndex = indexFrame;
    }

    [Header("Debug")]
    public int m_index;
    public void TakeScreenshotsAround() {
        m_index = 0;
        m_isRecording = true;
        StartCoroutine(Coroutine_TakeSceenShotAround());
    
    }

    public void SetFileNameFormat(string name)
    {
        m_fileNameFormat = name;
    }

    private IEnumerator Coroutine_TakeSceenShotAround()
    {
        m_isRecording = true;
        float timeScale = Time.timeScale;
        if (m_pauseGameWhileScreening)
            Time.timeScale = 0;
        while (m_index < m_requestAngle) { 
            yield return m_screenManager.TakeScreenShotCoroutine(string.Format("{0:0000}", m_startIndex+m_index), m_fileNameFormat,ScreenshotManager.ScreenshotType.PNG);
            m_index++;
            m_turnAround.SetCameraRotation(((float)m_index)*(360f / (float)m_requestAngle), m_verticalAngle);
            yield return new WaitForEndOfFrame();
            m_onChanged.Invoke((float)m_index / (float)m_requestAngle);
        }
        if (m_pauseGameWhileScreening && m_exitPauseAfternSceeening)
            Time.timeScale = timeScale;
        m_isRecording = false;
    }

    public string GetExtensionUsedAsString()
    {
        return ".png" ;
    }

    public int GetFramesCount()
    {
        return m_requestAngle;
    }

    [System.Serializable]
    public class ScreenshotStateEvent : UnityEvent<float> { }
}
