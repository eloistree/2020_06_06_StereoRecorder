﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.Events;

public class ScreenshotManager : MonoBehaviour, I_UseHarddriveSave {

    public string m_folderName = "UnityScreenshot";
    
    [Range(0f, 10f)]
    public int m_resolutionMulticator = 1;
    public UnityEvent _beforeScreenshot;
    public UnityEvent _afterScreenshot;
    public bool m_useHarddriveSave;

    public void TakeScreenShot()
    {
        StartCoroutine(TakeScreenShotCoroutine(DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_")));
    }
    public void TakeScreenShot(int indexName)
    {

        StartCoroutine(TakeScreenShotCoroutine("" + indexName));
    }
    public void TakeScreenShot(string idName)
    {

        StartCoroutine(TakeScreenShotCoroutine("" + idName));
    }
    public enum ScreenshotType { PNG, JPG, JPEG }
    public IEnumerator TakeScreenShotCoroutine(string idValue, string screenshotName = "Screenshot_", ScreenshotType type = ScreenshotType.PNG)
    {
        _beforeScreenshot.Invoke();
        yield return new WaitForEndOfFrame();
        string fileNameExt = string.Format("{0}{1}.{2}", screenshotName, idValue, type.ToString().ToLower());
        string folderPath = UnityDirectoryStorage.GetAbsolutePathFor(m_folderName,m_useHarddriveSave);
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);
        string filePath = UnityDirectoryStorage.GetAbsolutePathFor(m_folderName, fileNameExt, m_useHarddriveSave);
        if (File.Exists(filePath))
            File.Delete(filePath);
        ScreenCapture.CaptureScreenshot(filePath, m_resolutionMulticator);
        _afterScreenshot.Invoke();

    }


    public void SetSwitchTo(bool useHarddrive)
    {
        m_useHarddriveSave = useHarddrive;
    }

    public bool IsUsingHarddriveSave()
    {
        return m_useHarddriveSave;
    }

}
