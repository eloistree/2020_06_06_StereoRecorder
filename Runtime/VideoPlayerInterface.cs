﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class VideoPlayerInterface : MonoBehaviour
{
    public VideoPlayer m_linkedVideo;
    public bool m_autoFoundIfNull = true;

    public void RefreshWithURL(string url) {

        if (!CheckForVideo()) return;
        m_linkedVideo.enabled=(false);
        m_linkedVideo.url = url;
        m_linkedVideo.enabled = (true);
        Debug.Log("L: " + url);
    }
    public void SetVideoToPourcent(float pourcent)
    {

        if (!CheckForVideo()) return;

        m_linkedVideo.frame = VideoPlayerUtility.GetFrameByPourcent(m_linkedVideo, pourcent);
    }
    public void SwitchPauseState()
    {

        if (!CheckForVideo()) return;
        VideoPlayerUtility.SwitchPause(m_linkedVideo);
    }
    public void PauseVideo() { if (!CheckForVideo()) return; m_linkedVideo.Pause(); }
    public void PlayVideo() { if (!CheckForVideo()) return; m_linkedVideo.Play(); }


    private bool CheckForVideo()
    {
        if (m_linkedVideo == null)
        {
            if (m_autoFoundIfNull) {
                m_linkedVideo = GameObject.FindObjectOfType<VideoPlayer>();
            }
        }
        return m_linkedVideo != null;
    }

    private void Reset()
    {
        m_linkedVideo = GetComponent<VideoPlayer>();
    }

}
